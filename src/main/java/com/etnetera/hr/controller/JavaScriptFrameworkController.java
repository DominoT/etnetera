package com.etnetera.hr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;

/**
 * Simple REST controller for accessing application logic.
 * 
 * @author Etnetera
 *
 */
@RestController
public class JavaScriptFrameworkController {

	private final JavaScriptFrameworkRepository repository;

	@Autowired
	public JavaScriptFrameworkController(JavaScriptFrameworkRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/frameworks")
	public Iterable<JavaScriptFramework> frameworks() {
		return repository.findAll();
	}

	@GetMapping("/find-framework/{frameworkName}")
	public JavaScriptFramework findFramework(@PathVariable("frameworkName") String frameworkName) {
		for(JavaScriptFramework framework : repository.findAll()){
			if(framework.getName().equals(frameworkName))
				return framework;
		}
		return null;
	}

	@PostMapping("/store-framework")
	public JavaScriptFramework storeFramework(@NonNull @RequestBody JavaScriptFramework framework) {
		return repository.save(framework);
	}

	@PostMapping("/delete-framework")
	public void deleteFramework(@NonNull @RequestBody JavaScriptFramework framework) {
		repository.delete(framework);
	}

}

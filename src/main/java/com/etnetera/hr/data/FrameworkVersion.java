package com.etnetera.hr.data;

import java.io.Serializable;
import java.sql.Date;

public class FrameworkVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date deprecationDate;

    private String hypeLevel;

    public FrameworkVersion(){ }

    public FrameworkVersion(Date deprecationDate, String hypeLevel){
        this.deprecationDate = deprecationDate;
        this.hypeLevel = hypeLevel;
    }

    public Date getDeprecationDate() { return deprecationDate; }

    public void setDeprecationDate(Date deprecationDate) { this.deprecationDate = deprecationDate; }

    public String getHypeLevel() { return hypeLevel; }

    public void setHypeLevel(String hypeLevel) { this.hypeLevel = hypeLevel; }

    @Override
    public String toString() {
        return String.format("[deprecated= %s, hype= %s]", deprecationDate, hypeLevel);
    }

}

package com.etnetera.hr.data;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 * 
 * @author Etnetera
 *
 */
@Entity
public class JavaScriptFramework {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, length = 30)
	private String name;

	@ElementCollection
	private Map<String, FrameworkVersion> versionMap = new HashMap<>();

	public JavaScriptFramework() {
	}

	public JavaScriptFramework(String name, Map<String, FrameworkVersion> versionMap) {
		this.name = name;
		this.versionMap = versionMap;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, FrameworkVersion> getVersionMap() {
		return versionMap;
	}

	public void setVersionMap(Map<String, FrameworkVersion> versionMap) {
		this.versionMap = versionMap;
	}

	@Override
	public String toString() {
		return String.format("JavaScriptFramework [id= %d, name= %s, versions= %s]", id, name, versionMap);
	}

}

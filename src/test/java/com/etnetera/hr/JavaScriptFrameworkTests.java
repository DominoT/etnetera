package com.etnetera.hr;

import com.etnetera.hr.data.FrameworkVersion;
import com.etnetera.hr.data.JavaScriptFramework;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.sql.Date;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Class used for Spring Boot/MVC based tests.
 * 
 * @author Etnetera
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JavaScriptFrameworkTests extends AbstractTest {

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void aInitTestEmptyRepo() throws Exception {
        checkFrameworksSize(0);
    }

    @Test
    public void bAddNewSingleFramework() throws Exception {
        Map<String, FrameworkVersion> versionMap = new HashMap<>();
        versionMap.put("1.0.0", new FrameworkVersion(new Date(1662484219000L), "ZASTARALA: Verzia so zakladnymi kniznicami."));
        versionMap.put("1.0.1", new FrameworkVersion(null, "Verzia obsahujuca vsetky nove featury."));

        storeFramework(new JavaScriptFramework("Angular", versionMap));
        checkFrameworksSize(1);
    }

    @Test
    public void cFindAngularFramework() throws Exception {
        JavaScriptFramework framework = findFramework("Angular");
        assertEquals(1, (long) framework.getId());
        assertEquals(framework.getName(), "Angular");
    }

    @Test
    public void dAddTwoFrameworksAndRemoveVueJs() throws Exception {
        storeFramework(new JavaScriptFramework("Vue.js", Collections.singletonMap("0.0.9", new FrameworkVersion(new Date(1662484219000L), "Uzasna nova verzia na testovanie."))));
        storeFramework(new JavaScriptFramework("React", Collections.singletonMap("1.5.2", new FrameworkVersion(null, "Najlepsi framework vsetkych cias!"))));

        checkFrameworksSize(3);
        deleteFramework(findFramework("Vue.js"));
        checkFrameworksSize(2);
    }

    @Test
    public void eAddNewFrameworkAndEditExistingAngularFramework() throws Exception {
        JavaScriptFramework angularFramework = findFramework("Angular");
        assertEquals(angularFramework.getVersionMap().size(), 2);
        assertNull(angularFramework.getVersionMap().get("1.0.1").getDeprecationDate());

        // first, set deprecated date to 11 October and edit previous version
        Map<String, FrameworkVersion> versionMap = angularFramework.getVersionMap();
        FrameworkVersion frameworkVersion = versionMap.get("1.0.1");
        frameworkVersion.setDeprecationDate(new Date(1665484219000L));
        frameworkVersion.setHypeLevel("ZASTARALA: " + frameworkVersion.getHypeLevel());
        versionMap.put("1.0.1", frameworkVersion);

        // now, add new version without deprecationDate => actual version
        versionMap.put("1.0.2", new FrameworkVersion(null, "Verzia obsahujuca vsetky nove featury a best practises v roku 2023."));
        angularFramework.setVersionMap(versionMap);
        storeFramework(angularFramework);

        checkFrameworksSize(2);
        angularFramework = findFramework("Angular");
        assertEquals(angularFramework.getVersionMap().size(), versionMap.size());
        assertNotNull(angularFramework.getVersionMap().get("1.0.1").getDeprecationDate());
    }

    private void checkFrameworksSize(int expectedSize) throws Exception {
        String uri = "/frameworks";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        JavaScriptFramework[] frameworks = super.mapFromJson(content, JavaScriptFramework[].class);
        assertEquals(expectedSize, frameworks.length);
    }

    private JavaScriptFramework storeFramework(JavaScriptFramework framework) throws Exception {
        return checkStatusAndReturnObject(mvc.perform(MockMvcRequestBuilders.post("/store-framework")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(super.mapToJson(framework))).andReturn());
    }

    private JavaScriptFramework findFramework(String name) throws Exception {
        String uri = "/find-framework/" + name;
        return checkStatusAndReturnObject(mvc.perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn());
    }

    private JavaScriptFramework checkStatusAndReturnObject(MvcResult mvcResult)  throws Exception {
        assertEquals(200, mvcResult.getResponse().getStatus());
        String content = mvcResult.getResponse().getContentAsString();
        return super.mapFromJson(content, JavaScriptFramework.class);
    }

    private void deleteFramework(JavaScriptFramework framework) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/delete-framework")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(super.mapToJson(framework))).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());
    }

}
